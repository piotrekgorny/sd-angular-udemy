import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  detailsVisible: boolean = false;
  logs: Array<string> = [];

  onDisplayDetails():void {
    this.detailsVisible = !this.detailsVisible;
    this.logs.push((new Date()).toString());
  }
}

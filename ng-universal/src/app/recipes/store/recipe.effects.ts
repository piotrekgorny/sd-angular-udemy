import { Effect } from "@ngrx/effects";
import { Actions } from "@ngrx/effects";
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';


import * as RecipeActions from '../store/recipe.actions';
import { Recipe } from "../recipe.model";
import { Http } from "@angular/http";
import { Response } from "@angular/http";
import { Store } from "@ngrx/store";
import * as fromRecipe from '../store/recipe.reducers';
import * as fromAuth from '../../auth/store/auth.reducers';
import { Injectable } from "@angular/core";

@Injectable()
export class RecipeEffects {
    token: string;

    @Effect()
    recipeFetch = this.actions$
        .ofType(RecipeActions.FETCH_RECIPES)
        .switchMap((action: RecipeActions.FetchRecipes) => {
            return this.http.get('https://udemy-ng-http-db5d2.firebaseio.com/recipes.json?auth=' + this.token)
                
        })
        .map(
            (response: Response) => {
                const recipes: Recipe[] = response.json();
                for (let recipe of recipes) {
                    if (!recipe['ingredients']) {
                        recipe['ingredients'] = [];
                    }
                }

                return {
                    type: RecipeActions.SET_RECIPES,
                    payload: recipes
                };
            });
      
    @Effect({dispatch: false})
    recipeStore = this.actions$
        .ofType(RecipeActions.STORE_RECIPES)
        .withLatestFrom(this.store.select('recipes'))
        .switchMap(([action, state]) => {
            return this.http.put('https://udemy-ng-http-db5d2.firebaseio.com/recipes.json?auth=' + this.token, state.recipes);
        })

    constructor(private actions$: Actions, private http: Http, private store: Store<fromRecipe.FeatureState>) {
        this.store.select('auth').subscribe((authState: fromAuth.State) => {
            this.token = authState.token;
        });
    }
}
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.sort((value1: any, value2: any) => {
      return (<string>value1.name).localeCompare(value2.name);
    })
  }

}

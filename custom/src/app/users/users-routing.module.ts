import { NgModule } from "@angular/core";
import { Routes } from '@angular/router';

import { RouterModule } from "@angular/router";
import { UsersComponent } from "./users.component";



@NgModule({
    imports: [
        RouterModule.forChild([{
            path: '',
            component: UsersComponent
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class UsersRoutingModule {

}
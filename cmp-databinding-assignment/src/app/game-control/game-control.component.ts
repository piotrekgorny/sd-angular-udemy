import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  intervalHandler: number = 0;
  gameTickCounter: number = 1;
  @Output() tick = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onGameStart() {
    this.intervalHandler = window.setInterval(
      () => {
        this.tick.emit(this.gameTickCounter);
        this.gameTickCounter++;
      }, 1000);
  }

  onGameStop() {
    window.clearInterval(this.intervalHandler);
    this.intervalHandler = 0;
  }

  isGameRunning(): boolean {
    return this.intervalHandler != 0;
  }

}

import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { RecipeService } from "../recipes/recipe.service";
import { Recipe } from "../recipes/recipe.model";
import "rxjs/Rx";
import { AuthService } from "../auth/auth.service";

@Injectable()
export class DataStorageService {
    constructor(private http: Http, private rerecipeService: RecipeService, private authService: AuthService) {

    }

    public storeRecipes() {
        const token = this.authService.getToken();

        return this.http.put('https://udemy-ng-http-db5d2.firebaseio.com/recipes.json?auth=' + token, this.rerecipeService.getRecipes());
    }

    public getRecipes() {
        const token = this.authService.getToken();

        this.http.get('https://udemy-ng-http-db5d2.firebaseio.com/recipes.json?auth=' + token)
            .map(
                (response: Response) => {
                    const recipes: Recipe[] = response.json();
                    for (let recipe of recipes) {
                        if (!recipe['ingredients']) {
                            recipe['ingredients'] = [];
                        }
                    }

                    return recipes;
                }
            ).subscribe(
                (recipes: Recipe[]) => {
                    this.rerecipeService.setRecipes(recipes);
                }
            );
    }
}
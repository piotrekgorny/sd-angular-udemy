import { Injectable } from "@angular/core";
import { RecipeService } from "../recipes/recipe.service";
import { Recipe } from "../recipes/recipe.model";
import "rxjs/Rx";
import { AuthService } from "../auth/auth.service";
import { HttpClient, HttpHeaders, HttpParams, HttpRequest} from "@angular/common/http";

@Injectable()
export class DataStorageService {
    constructor(private httpClient: HttpClient, private rerecipeService: RecipeService, private authService: AuthService) {

    }

    public storeRecipes() {
        // return this.httpClient.put('https://udemy-ng-http-db5d2.firebaseio.com/recipes.json', this.rerecipeService.getRecipes(), {
        //     observe: 'body',
        //     params: new HttpParams().set('auth', token)
        //     // headers: new HttpHeaders().set('Test', 'Value')
        // });

        const req = new HttpRequest(
            'PUT', 
            'https://udemy-ng-http-db5d2.firebaseio.com/recipes.json',
            this.rerecipeService.getRecipes(), 
            {
                reportProgress: true
            }
        );

        return this.httpClient.request(req);
    }

    public getRecipes() {
        const token = this.authService.getToken();

        // this.httpClient.get<Recipe[]>('https://udemy-ng-http-db5d2.firebaseio.com/recipes.json?auth=' + token)
        this.httpClient.get<Recipe[]>('https://udemy-ng-http-db5d2.firebaseio.com/recipes.json', {
                observe: 'body',
                responseType: 'json'
            })
            .map(
                (recipes) => {
                    for (let recipe of recipes) {
                        if (!recipe['ingredients']) {
                            recipe['ingredients'] = [];
                        }
                    }

                    return recipes;
                }
            ).subscribe(
                (recipes: Recipe[]) => {
                    this.rerecipeService.setRecipes(recipes);
                }
            );
    }
}
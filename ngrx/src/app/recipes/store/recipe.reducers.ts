import { Recipe } from "../recipe.model";
import { Ingredient } from "../../shared/ingredient.model";
import * as RecipeActions from "./recipe.actions";
import { race } from "rxjs/operator/race";
import * as fromApp from '../../store/app.reducers';

export interface FeatureState extends fromApp.AppState {
    recipes: State
}

export interface State {
    recipes: Recipe[]
}

const initialState: State = {
    recipes: [
        new Recipe(
            "Pizza", 
            "Best Italian style home baked pizza",
            "https://www.campbellsoup.co.uk/img/recipes/6-campbells-vegetarian-pizza-recipe.jpg",
            [
                new Ingredient("Flour", 1),
                new Ingredient("Water", 1)
            ]
        ),
        new Recipe(
            "Green to go",
            "Fresh & Healthy",
            "http://www.seriouseats.com/images/2016/05/20160503-fava-carrot-ricotta-salad-recipe-1.jpg",
            [
                new Ingredient("Bread", 1),
                new Ingredient("Olives", 10)
            ]
        )
    ]
};

export function recipeReducer(state = initialState, action: RecipeActions.RecipeActions) {

    switch (action.type) {
        case (RecipeActions.SET_RECIPES):
        {
            return {
                ...state,
                recipes: [...action.payload]
            };
        }
        case (RecipeActions.ADD_RECIPE):
        {
            return {
                ...state,
                recipes: [...state.recipes, action.payload]
            };
        }
        case (RecipeActions.UPDATE_RECIPE):
        {
            const recipe = state.recipes[action.payload.index];
            const updatedRecipe = {
                ...recipe,
                ...action.payload.updatedRecipe
            };

            const recipes = [...state.recipes];
            recipes[action.payload.index] = updatedRecipe;
            return {
                ...state,
                recipes: recipes
            };
        }
        case (RecipeActions.DELETE_RECIPE):
        {
            const oldRecipes = [...state.recipes];
            oldRecipes.splice(action.payload, 1);

            return {
                ...state,
                recipes: oldRecipes
            }
        }

        default: 
            return state;
    }
}
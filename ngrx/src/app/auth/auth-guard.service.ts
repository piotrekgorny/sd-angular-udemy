import { CanActivate } from "@angular/router";
import { ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router/src/router_state";
import { Injectable } from "@angular/core";
import * as fromApp from '../store/app.reducers';
import { Store } from "@ngrx/store";
import * as fromAuth from './store/auth.reducers';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private store: Store<fromApp.AppState>) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.store.select('auth')
        .take(1)
        .map(
            (authState: fromAuth.State) => {
                return authState.authenticated;
            } 
        );
    }
}
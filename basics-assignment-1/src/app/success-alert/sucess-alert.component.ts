import { Component } from "@angular/core";

@Component({
    selector: "app-success-alert, .success",
    template: `
    <p>
        This is success alert!
    </p>`,
    styles: [`
    p {
        color: white;
        background-color: green;
        text-align: center;
        padding: 1em;
        font-weight: bold;
    }
    `]
})
export class SuccessAlertComponent {}
import { Component } from '@angular/core';
import { UsersService } from './services/users.service';
import { CounterService } from './services/counter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  get activationCount(): number {
    return this.counterService.activationCounter;
  }

  get deactivationCount(): number {
    return this.counterService.deactivationCounter;
  }

  constructor(private usersService: UsersService, private counterService: CounterService) {
    
  }
}

import { Injectable } from "@angular/core";
import { UsersService } from "./users.service";

@Injectable()
export class CounterService {
    activationCounter: number = 0;
    deactivationCounter: number = 0;

    constructor(private usersService: UsersService) {
        this.usersService.activated.subscribe(() => this.activationCounter++);
        this.usersService.inactivated.subscribe(() => this.deactivationCounter++);
    }
}
import { EventEmitter } from "@angular/core";

export class UsersService {
    activeUsers = ['Max', 'Anna'];
    inactiveUsers = ['Chris', 'Manu'];
    activated = new EventEmitter<void>();
    inactivated = new EventEmitter<void>();

    setActive(user: string) {
        if (!this.inactiveUsers.includes(user)) {
            return;
        }

        this.inactiveUsers.splice(this.inactiveUsers.indexOf(user), 1);
        this.activeUsers.push(user);
        this.activated.emit();
    }

    setInactive(user: string) {
        if (!this.activeUsers.includes(user)) {
            return;
        }

        this.activeUsers.splice(this.activeUsers.indexOf(user), 1);
        this.inactiveUsers.push(user);
        this.inactivated.emit();
    }
}